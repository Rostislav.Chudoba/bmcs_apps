{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# 4.4 Bond-slip elasto-plastic model with isotropic and kinematic hardening \n",
    "This notebook generalizes the implementation shown in notebook 4.2  time integration algorithm for plasticity modes. Instead of explicitly introducing individual state variables it uses vectors that can represent both different types of state representation and also different dimensions. In this way, a reader is introduced to a more abstract representation of the model components which is the basis for the next step, an introduction of thermodynamically based models. The ingredients of a thermodynamically based material model are\n",
    "\n",
    " - Vector of state variables $\\boldsymbol{\\mathcal{E}}$\n",
    " - Vector of streses $\\boldsymbol{\\mathcal{S}}$\n",
    " - Yield condition  $f(\\boldsymbol{\\mathcal{S}},\\boldsymbol{\\mathcal{E}})$\n",
    " - Flow potential $\\varphi(\\boldsymbol{\\mathcal{S}},\\boldsymbol{\\mathcal{E}})$\n",
    "\n",
    "As we consider only associative plasticity in this notebook, $\\varphi = f$.\n",
    "\n",
    "The time-stepping algorithm gets generated automatically within the thermodynamically framework. The derived  evolution equations and return-mapping to the yield surface is performed using Newton scheme.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import sympy as sp\n",
    "sp.init_printing()\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model components"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Material parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "E_b = sp.Symbol('E_b', real=True, nonnegative=True)\n",
    "gamma = sp.Symbol('gamma', real=True, nonnegative=True)\n",
    "K = sp.Symbol('K', real=True)\n",
    "tau_bar = sp.Symbol(r'\\tau_{\\mathrm{Y}}', real=True, nonnegative=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "py_vars = ('E_b', 'gamma', 'K', 'tau_bar')\n",
    "map_py2sp = {py_var : globals()[py_var] for py_var in py_vars}\n",
    "sp_vars = tuple(map_py2sp[py_var] for py_var in py_vars)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "&nbsp;<font color='blue'>\n",
    "**Names of mathematical symbols in `sympy`:**\n",
    "    \n",
    "Mathematical symbols defined as string in `sp.symbols(r'\\tau')` use `latex` syntax to introduce greek symbols, super and subindexes. This makes the pretty printing of expression possible. However, leads sometimes to problems when generating executable functions using `sp.lambdify`. Because latex symbols can contain special characters not allowed for latex symbols, this might lead to problems. This issue is fixed here using naming dictionaries `map_py2sp` and two ordered tuples `py_vars` and `sp_vars` containing the `sympy` symbols and string names of the symbols. They are needed to enable the generation of executable `Python` or `C` code. This will be simplified in future releases. \n",
    "</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### State variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "s = sp.Symbol('s', real=True)\n",
    "s_pi = sp.Symbol(r's_pi', real=True)\n",
    "alpha = sp.Symbol('alpha', real=True)\n",
    "z = sp.Symbol('z', real=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "Eps = sp.Matrix([s_pi, z, alpha])\n",
    "Eps.T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Conjugate stress variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "tau = sp.Symbol('tau', real=True)\n",
    "X = sp.Symbol('X', real=True)\n",
    "Z = sp.Symbol('Z', real=True, nonnegative=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "Sig = sp.Matrix([tau, Z, X])\n",
    "Sig.T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The constitutive laws relating the conjugate forces and kinematic state variables are defined as follows"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Sig_Eps = sp.Matrix([[E_b * (s - s_pi), K * z, alpha * gamma]]).T\n",
    "Sig_Eps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Executable code for** $\\boldsymbol{\\mathcal{S}}(s,\\boldsymbol{\\mathcal{E}})$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_Sig = sp.lambdify(\n",
    "    (s, Eps) + sp_vars, Sig_Eps.T, 'numpy'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "To derive the time stepping procedure we will need also the matrix of derivatives of the generalized stresses $\\boldsymbol{\\mathcal{S}}$ with respect to the kinematic variables $\\boldsymbol{\\mathcal{E}}$ \n",
    "\\begin{align}\n",
    "\\frac{\\partial \\boldsymbol{\\mathcal{S}}}{\\partial \\boldsymbol{\\mathcal{E}}}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "dSig_dEps = sp.Matrix([Sig_Eps.T.diff(eps) for eps in Eps]).T\n",
    "dSig_dEps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Executable Python code generation** $\\displaystyle{\\frac{\\partial \\boldsymbol{\\mathcal{S}}}{\\partial \\boldsymbol{\\mathcal{E}}}}(s,\\boldsymbol{\\mathcal{E}})$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [],
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "get_dSig_dEps = sp.lambdify(\n",
    "    (s, Eps) + sp_vars, dSig_dEps, 'numpy'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Threshold function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "To keep the framework general for different stress norms and hardening definitions let us first introduce a general function for effective stress. Note that the observable stress $\\tau$ is identical with the plastic stress $\\tau_\\pi$ due to the performed sign switch in the definition of the thermodynamic forces."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "f_Sig = sp.sqrt((tau - X)*(tau - X)) - (tau_bar + Z)\n",
    "f_Sig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Executable code generation** $f(\\boldsymbol{\\mathcal{E}}, \\boldsymbol{\\mathcal{S}})$\n",
    "\n",
    "Note that this is a function of both the forces and kinematic state variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "get_f_Sig = sp.lambdify(\n",
    "    (Eps, Sig) + sp_vars, f_Sig, 'numpy'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The derivative of $f$ required for time-stepping $\\partial_\\boldsymbol{\\mathcal{S}} f$ is obtained as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df_dSig = f_Sig.diff(Sig)\n",
    "sp.simplify(df_dSig).T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Executable code generation** $\\partial_\\boldsymbol{\\mathcal{S}}f(\\boldsymbol{\\mathcal{E}}, \\boldsymbol{\\mathcal{S})}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "get_df_dSig = sp.lambdify(\n",
    "    (Eps, Sig) + sp_vars, df_dSig, 'numpy'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Flow direction\n",
    "Evolution equations have the form\n",
    "\\begin{align}\n",
    " \\dot{\\boldsymbol{\\mathcal{E}}} = \\lambda \\, \\boldsymbol{\\Phi}\n",
    "\\end{align}\n",
    "with the vector $\\boldsymbol{\\Phi}$ representing the flow direction within \n",
    "the stress space. Assuming the normality condition, i.e. that \n",
    "the flow direction coincides with the vector normal to the \n",
    "yield condition we can write\n",
    "\\begin{align}\n",
    " \\boldsymbol{\\Phi} = \\boldsymbol{\\Upsilon} \\frac{\\partial f}{\\partial \\boldsymbol{\\mathcal{S}}}\n",
    "\\end{align}\n",
    "The sign matrix $\\boldsymbol{\\Upsilon}$ is used to direct all the derivatives in the outward direction from the elastic range."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Sig_signs = sp.diag(-1,1,1)\n",
    "Phi = -Sig_signs * df_dSig\n",
    "Phi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_Phi = sp.lambdify(\n",
    "    (Eps, Sig) + sp_vars, Phi, 'numpy'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Time integration scheme"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Summary of the backward Euler scheme\n",
    "The derived expressions can be now plugged-in into a generic return mapping algorithm that efficiently identifies a state that satisfies the discrete consistency condition. The general structure of an implicit integration scheme reads\n",
    "\\begin{align}\n",
    "\\boldsymbol{\\mathcal{E}}_{n+1} &= \\boldsymbol{\\mathcal{E}}_{n} +  \n",
    "\\lambda \\, \\boldsymbol{\\Phi}_{n+1} \\\\\n",
    "f(\\boldsymbol{\\mathcal{E}}_{n+1};  \\lambda) &= 0\n",
    "\\end{align}\n",
    "To reach an admissible state let us linearize the threshold function at an intermediate state $k$ as\n",
    "\\begin{align}\n",
    "f(\\boldsymbol{\\mathcal{E}}^{(k)}; \\lambda^{(k)} )\n",
    "& \\approx\n",
    "f^{(k)} \n",
    " + \n",
    "\\left. \\frac{\\partial f}{\\partial \\lambda} \\right|^{(k)}\n",
    "\\Delta \\lambda\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Thus, by rewriting the linearized equation as a recurrence formula, the iteration algorithm is obtained\n",
    "\\begin{align}\n",
    "&\\left. \\frac{\\partial f}{\\partial \\lambda} \\right|_{k}\n",
    "\\Delta \\lambda =\n",
    "- f_{k}\n",
    "\\\\\n",
    "&\\lambda_{k+1} = \\lambda_{k} + \\Delta \\lambda \\\\\n",
    "& \\boldsymbol{\\mathcal{E}}_{k+1}  = \n",
    "\\boldsymbol{\\mathcal{E}}_{k} + \n",
    " \\lambda \\, \\boldsymbol{\\Phi}_{k}\n",
    " \\\\\n",
    "&k = k + 1\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "To define a generic return mapping we need to construct the  derivatives of the flow rule $f$ with respect to $\\lambda$. The dependency of $f$ on $\\lambda$ is intermediated via the stresses $\\boldsymbol{\\mathcal{S}}$ and state variables $\\boldsymbol{\\mathcal{E}}$\n",
    "\\begin{align}\n",
    "f(\\boldsymbol{\\mathcal{S}}(\\boldsymbol{\\mathcal{E}}(\\lambda))).\n",
    "\\end{align}\n",
    "To correctly resolve the dependencies in the derivative $\\frac{\\partial f}{\\partial_\\lambda}$, we need to apply rules for chaining of derivatives. Let us start with the derivative with respect to $\\boldsymbol{\\mathcal{E}}$ in the form\n",
    "\\begin{align}\n",
    "\\frac{\\partial f(\\boldsymbol{\\mathcal{S}}(\\boldsymbol{\\mathcal{E}}))}{\\partial \\boldsymbol{\\mathcal{E}}}\n",
    " &=\n",
    "\\frac{\\partial f(\\boldsymbol{\\mathcal{S}})}{\\partial \\boldsymbol{\\mathcal{S}}}   \\, \n",
    "\\frac{\\partial \\boldsymbol{\\mathcal{S}}(\\boldsymbol{\\mathcal{E}})}{\\partial \\boldsymbol{\\mathcal{E}}}.\n",
    "\\end{align}\n",
    "By expanding the derivatives of $\\boldsymbol{\\mathcal{E}}$ with respect to $\\lambda_\\Delta$ that will be abbreviate in index position as $\\lambda$ for brevity we obtain\n",
    "\\begin{align}\n",
    "\\frac{\\partial f(\\boldsymbol{\\mathcal{S}}(\\boldsymbol{\\mathcal{E}}(\\lambda)))}{\\partial \\lambda}\n",
    " &=\n",
    "\\frac{\\partial f}{\\partial \\boldsymbol{\\mathcal{S}}}   \\, \n",
    "\\frac{\\partial \\boldsymbol{\\mathcal{S}}}{\\partial \\boldsymbol{\\mathcal{E}}}\n",
    "\\frac{\\partial \\boldsymbol{\\mathcal{E}}}{\\partial \\lambda}.\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The last term $\\frac{\\partial \\boldsymbol{\\mathcal{E}} }{ \\partial \\lambda}$ can be obtained from the evolution equations\n",
    "\\begin{align}\n",
    "\\boldsymbol{\\mathcal{E}} = \\lambda \\, \\boldsymbol{\\Phi} \\; \\implies\n",
    "\\frac{\\partial \\boldsymbol{\\mathcal{E}} }{\\partial \\lambda} = \n",
    " \\boldsymbol{\\Phi}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Summarizing**: the algorithm can be written in a compact way as follows:\n",
    "\\begin{align}\n",
    "&\n",
    "\\left(\n",
    "\\frac{\\partial f}{\\partial \\boldsymbol{\\mathcal{S}}}\n",
    "^{(k)} \\, \n",
    "\\frac{\\partial \\boldsymbol{\\mathcal{S}}}{\\partial \\boldsymbol{\\mathcal{E}}}\n",
    "^{(k)} \\, \n",
    "\\boldsymbol{\\Phi}^{(k)}\n",
    "\\right)\n",
    "\\Delta \\lambda = -\n",
    "f^{(k)}\\\\\n",
    "&\\lambda_{\\Delta}^{(k+1)} = \\lambda_{\\Delta}^{(k)} + \\Delta \\lambda \\\\\n",
    "& \\boldsymbol{\\mathcal{E}}^{(k+1)} = \\boldsymbol{\\mathcal{E}}^{(k)} + \n",
    " \\lambda_\\Delta \\, \\boldsymbol{\\Phi}^{(k)}\n",
    " \\\\\n",
    "&k = k + 1\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Implementation concept\n",
    "The gradient operators needed for the time-stepping scheme have been derived above and are now available for the implementation of the numerical algorithm both in `Python` and `C89` languages\n",
    "\n",
    "<table style=\"width:50%\">\n",
    "<tr>\n",
    "<th>Symbol</th>\n",
    "<th>Python</th>\n",
    "<th>C89</th>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>$\\mathcal{S}(\\boldsymbol{\\mathcal{E}}) $  \n",
    "</td>\n",
    "<td>get_Sig</td>\n",
    "<td>get_Sig_C</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>$ f(\\boldsymbol{\\mathcal{S}}, \\boldsymbol{\\mathcal{E}})$</td>\n",
    "<td>get_f</td>\n",
    "<td>get_f_C</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>\n",
    "$\\displaystyle{\\frac{\\partial f}{\\partial \\boldsymbol{\\mathcal{S}}}}(\\boldsymbol{\\mathcal{S}}, \\boldsymbol{\\mathcal{E}})$\n",
    "    </td>\n",
    "<td>get_df_dSig</td>\n",
    "<td>get_df_dSig_C</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>\n",
    "$\\displaystyle{\\frac{\\partial \\boldsymbol{\\mathcal{S}}}{\\partial \\boldsymbol{\\mathcal{E}}}}(\\boldsymbol{\\mathcal{E}})$</td>\n",
    "<td>get_dSig_dEps</td>\n",
    "<td>get_dSig_dEps_C</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td>$\\boldsymbol{\\Phi}(\\boldsymbol{\\mathcal{S}}, \\boldsymbol{\\mathcal{E}}) $</td>\n",
    "<td>get_Phi</td>\n",
    "<td>get_Phi_C</td>\n",
    "</tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "To avoid repeated calculation of the same expressions, let us put the evaluation of $f$ and $\\frac{\\partial f}{\\partial \\lambda}$ into a single procedure. Indeed, the iteration loop can be constructed in such a way that the predictor $\\frac{\\partial f}{\\partial \\lambda}$ for the next step is calculated along with the residuum $f$. In case that the residuum is below the required tolerance, the overhead for an extra calculated derivative is negligible or, with some care, it can be even reused in the next time step.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def get_f_df(s_n1, Eps_k, *margs):\n",
    "    Sig_k = get_Sig(s_n1, Eps_k, *margs)[0]\n",
    "    f_k = np.array([get_f_Sig(Eps_k, Sig_k, *margs)])\n",
    "    df_dSig_k = get_df_dSig(Eps_k, Sig_k, *margs)\n",
    "    Phi_k = get_Phi(Eps_k, Sig_k, *margs)\n",
    "    dSig_dEps_k = get_dSig_dEps(s_n1, Eps_k, *margs)\n",
    "    df_dSigEps_k = np.einsum(\n",
    "        'ik,ji->jk', df_dSig_k, dSig_dEps_k)\n",
    "    dEps_dlambda_k = Phi_k\n",
    "    df_dlambda = np.einsum(\n",
    "        'ki,kj->ij', df_dSigEps_k, dEps_dlambda_k)\n",
    "    df_k = df_dlambda\n",
    "    return f_k, df_k, Sig_k"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The update of state variables $\\boldsymbol{\\mathcal{E}}^{(k+1)}$ for an newly obtained $\\lambda_\\Delta^{(k+1)}$ is performed using the evolution equations. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def get_Eps_k1(s_n1, Eps_n, lam_k, Eps_k, *margs):\n",
    "    Sig_k = get_Sig(s_n1, Eps_k, *margs)[0]\n",
    "    Phi_k = get_Phi(Eps_k, Sig_k, *margs)\n",
    "    Eps_k1 = Eps_n + lam_k * Phi_k[:,0]\n",
    "    return Eps_k1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The double loop over the time increments and over the return mapping iteration. The inner loop represents the material point level in a standard finite element calculation. The input is the maximum slip value, the number of time steps, the maximum number of iterations and a load function which can define cyclic loading as shown below. The procedure returns the record of $\\boldsymbol{\\mathcal{E}}(t)$ and $\\boldsymbol{\\mathcal{S}}(t)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def get_response(margs, s_max=3, n_steps = 10, k_max=20, get_load_fn=lambda t: t):\n",
    "    Eps_n = np.zeros((len(Eps),), dtype=np.float_)\n",
    "    Eps_k = np.copy(Eps_n)\n",
    "    Sig_record, Eps_record, iter_record = [], [], []\n",
    "    t_arr = np.linspace(0,1,n_steps+1)\n",
    "    s_t = s_max * get_load_fn(t_arr) + 1e-9\n",
    "    for s_n1 in s_t:\n",
    "        lam_k = 0\n",
    "        f_k, df_k, Sig_k = get_f_df(s_n1, Eps_k, *margs)\n",
    "        f_k_norm = np.linalg.norm(f_k)\n",
    "        f_k_trial = f_k[-1]\n",
    "        k = 0\n",
    "        while k < k_max:\n",
    "            if f_k_trial < 0 or f_k_norm < 1e-8:\n",
    "                Eps_n[...] = Eps_k[...]\n",
    "                Sig_record.append(Sig_k)\n",
    "                Eps_record.append(np.copy(Eps_k))\n",
    "                iter_record.append(k+1)\n",
    "                break\n",
    "            dlam = np.linalg.solve(df_k, -f_k)\n",
    "            lam_k += dlam\n",
    "            Eps_k = get_Eps_k1(s_n1, Eps_n, lam_k, Eps_k, *margs)\n",
    "            f_k, df_k, Sig_k = get_f_df(s_n1, Eps_k, *margs)\n",
    "            f_k_norm = np.linalg.norm(f_k)\n",
    "            k += 1\n",
    "        else:\n",
    "            print('no convergence')\n",
    "    Sig_arr = np.array(Sig_record, dtype=np.float_)\n",
    "    Eps_arr = np.array(Eps_record, dtype=np.float_)\n",
    "    iter_arr = np.array(iter_record,dtype=np.int_)\n",
    "    return t_arr, s_t, Eps_arr, Sig_arr, iter_arr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Support functions\n",
    "To run some examples, let us define some infrastructure including a more complex loading history and postprocessing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Loading history\n",
    "This implementation uses the symbolic machinery which is not necessary a simpler data point based implementation with `numpy.interp1d` would be better ... later "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "t, theta = sp.symbols(r't, \\theta')\n",
    "n_cycles = 5\n",
    "A = 2\n",
    "ups = np.array([((theta-2*cycle)*A+(1-A), theta-2*cycle<=1) \n",
    "                for cycle in range(n_cycles)])\n",
    "downs = np.array([((1-(theta-(2*cycle+1)))*A+(1-A),(theta-(2*cycle+1))<=1) \n",
    "                  for cycle in range(n_cycles)])\n",
    "ups[0,0] = theta\n",
    "updowns = np.einsum('ijk->jik',np.array([ups, downs])).reshape(-1,2)\n",
    "load_fn = sp.Piecewise(*updowns).subs(theta,t*n_cycles)\n",
    "get_load_fn = sp.lambdify(t, load_fn,'numpy')\n",
    "t_arr = np.linspace(0,1,600)\n",
    "plt.plot(t_arr, get_load_fn(t_arr));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Plotting functions\n",
    "To simplify postprocessing examples, here are two aggregate plotting functions, one for the state and force variables, the other one for the evaluation of energies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def plot_Sig_Eps(t_arr, s_t, Sig_arr, Eps_arr, iter_arr, ax1, ax11, ax2, ax22, ax3, ax33):\n",
    "    colors = ['blue','red', 'green', 'black', 'magenta' ]\n",
    "    s_pi_, z_, alpha_ = Eps_arr.T\n",
    "    sig_pi_, Z_, X_ = Sig_arr.T\n",
    "    n_step = len(s_pi_)\n",
    "    ax1.plot(s_t, sig_pi_, color='black', \n",
    "             label='n_steps = %g' % n_step)\n",
    "    ax1.set_xlabel('$s$'); ax1.set_ylabel(r'$\\tau$')\n",
    "    ax1.legend()\n",
    "    if ax11:\n",
    "        ax11.plot(s_t, iter_arr, '-.')\n",
    "    ax2.plot(t_arr, z_, color='green', \n",
    "             label='n_steps = %g' % n_step)\n",
    "    ax2.set_xlabel('$t$'); ax2.set_ylabel(r'$z$')\n",
    "    if ax22:\n",
    "        ax22.plot(t_arr, Z_, '-.', color='green')\n",
    "        ax22.set_ylabel(r'$Z$')\n",
    "    ax3.plot(t_arr, alpha_, color='blue', \n",
    "             label='n_steps = %g' % n_step)\n",
    "    ax3.set_xlabel('$t$'); ax3.set_ylabel(r'$\\alpha$')\n",
    "    if ax33:\n",
    "        ax33.plot(t_arr, X_, '-.', color='blue')\n",
    "        ax33.set_ylabel(r'$X$')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "from scipy.integrate import cumtrapz\n",
    "def plot_work(ax, t_arr, s_t, Eps_arr, Sig_arr):\n",
    "    W_arr = cumtrapz(Sig_arr[:,0], s_t, initial=0)\n",
    "    U_arr = Sig_arr[:,0] * (s_t-Eps_arr[:,0]) / 2.0\n",
    "    G_arr = W_arr - U_arr\n",
    "    ax.plot(t_arr, W_arr, lw=2, color='black', label=r'$W$')\n",
    "    ax.plot(t_arr, G_arr, color='black', label=r'$G$')\n",
    "    ax.fill_between(t_arr, W_arr, G_arr, color='green', alpha=0.2)\n",
    "    ax.set_xlabel('$s$'); ax3.set_ylabel(r'$E$')\n",
    "    ax.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def plot_dissipation(ax, t_arr, s_t, Eps_arr, Sig_arr):    \n",
    "    colors = ['blue','red', 'green', 'black', 'magenta' ]\n",
    "    E_i = cumtrapz(Sig_arr, Eps_arr, initial=0, axis=0)\n",
    "    c = 'black'\n",
    "    ax.plot(t_arr, E_i[:,0], '-.', lw=1, color=c)\n",
    "    ax.fill_between(t_arr, E_i[:,0], 0, color=c, alpha=0.1)\n",
    "    c = 'black'\n",
    "    ax.plot(t_arr, E_i[:,0], color=c, lw=1)\n",
    "    ax.fill_between(t_arr, E_i[:,0], E_i[:,0], \n",
    "                    color=c, alpha=0.2);\n",
    "    c = 'blue'\n",
    "    ax.plot(t_arr, E_i[:,1], '-.', lw=1, color='black')\n",
    "    ax.fill_between(t_arr, E_i[:,1], 0, color=c, alpha=0.1)\n",
    "    c = 'blue'\n",
    "    ax.plot(t_arr, E_i[:,1] + E_i[:,2], color='black', lw=1)\n",
    "    ax.fill_between(t_arr, E_i[:,1] + E_i[:,2], E_i[:,1], \n",
    "                    color=c, alpha=0.3);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "material_params = {\n",
    "    E_b:1, gamma: 0.0, K:0.1, tau_bar:1, \n",
    "}\n",
    "margs = [material_params[map_py2sp[name]] for name in py_vars]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Monotonic load \n",
    "Let's first run the example with different size of the time step to see if there is any difference"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "fig, ((ax1,ax2,ax3)) = plt.subplots(1,3,figsize=(14,4), tight_layout=True)\n",
    "ax11, ax22, ax33 = ax1.twinx(), ax2.twinx(), ax3.twinx()\n",
    "for n_steps in [20, 40, 200, 2000]: \n",
    "    t_arr, s_t, Eps_arr, Sig_arr, iter_arr = get_response(\n",
    "        margs=margs, s_max=8, n_steps=n_steps, k_max=10\n",
    "    )\n",
    "    plot_Sig_Eps(t_arr, s_t, Sig_arr, Eps_arr, iter_arr, ax1, ax11, ax2, ax22, ax3, ax33)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Work supply and energy dissipation**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,1,figsize=(9, 5))\n",
    "plot_work(ax, t_arr, s_t, Eps_arr, Sig_arr)\n",
    "plot_dissipation(ax, t_arr, s_t, Eps_arr, Sig_arr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Cyclic loading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "fig, ((ax1,ax2,ax3)) = plt.subplots(1,3,figsize=(14,4), tight_layout=True)\n",
    "ax11,ax22,ax33 = ax1.twinx(), ax2.twinx(), ax3.twinx()\n",
    "t_arr, s_t, Eps_arr, Sig_arr, iter_arr = get_response(\n",
    "    margs, s_max=2, n_steps=20000, k_max=20, get_load_fn=get_load_fn\n",
    ")\n",
    "plot_Sig_Eps(t_arr, s_t, Sig_arr, Eps_arr, iter_arr, ax1, ax11, ax2, ax22, ax3, ax33);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,1,figsize=(9, 5))\n",
    "plot_work(ax, t_arr, s_t, Eps_arr, Sig_arr)\n",
    "plot_dissipation(ax, t_arr, s_t, Eps_arr, Sig_arr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hide_input": false,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Interactive application"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def init():\n",
    "    global Eps_record, Sig_record, iter_record, t_arr, s_t, s0, t0, Eps_n\n",
    "    s0 = 0\n",
    "    t0 = 0\n",
    "    Sig_record = []\n",
    "    Eps_record = []\n",
    "    iter_record = []\n",
    "    t_arr = []\n",
    "    s_t = []\n",
    "    Eps_n = np.zeros((len(Eps),), dtype=np.float_)\n",
    "    \n",
    "def get_response_i(s1, margs, n_steps = 60, k_max=20):\n",
    "    global Eps_record, Sig_record, iter_record, t_arr, s_t, s0, t0, Eps_n\n",
    "    Eps_k = np.copy(Eps_n)\n",
    "    t1 = t0+n_steps+1\n",
    "    ti_arr = np.linspace(t0, t1, n_steps+1 )\n",
    "    si_t = np.linspace(s0,s1,n_steps+1)\n",
    "    for s_n1 in si_t:\n",
    "        lam_k = 0\n",
    "        f_k, df_k, Sig_k = get_f_df(s_n1, Eps_k, *margs)\n",
    "        f_k_norm = np.linalg.norm(f_k)\n",
    "        f_k_trial = f_k[-1]\n",
    "        k = 0\n",
    "        while k < k_max:\n",
    "            if f_k_trial < 0 or f_k_norm < 1e-6:\n",
    "                Eps_n[...] = Eps_k[...]\n",
    "                Sig_record.append(Sig_k)\n",
    "                Eps_record.append(np.copy(Eps_k))\n",
    "                iter_record.append(k+1)\n",
    "                break\n",
    "            dlam = np.linalg.solve(df_k, -f_k)\n",
    "            lam_k += dlam\n",
    "            Eps_k = get_Eps_k1(s_n1, Eps_n, lam_k, Eps_k, *margs)\n",
    "            f_k, df_k, Sig_k = get_f_df(s_n1, Eps_k, *margs)\n",
    "            f_k_norm = np.linalg.norm(f_k)\n",
    "            k += 1\n",
    "        else:\n",
    "            print('no convergence')\n",
    "    t_arr = np.hstack([t_arr, ti_arr])\n",
    "    s_t = np.hstack([s_t, si_t])\n",
    "    t0 = t1\n",
    "    s0 = s1\n",
    "    return\n",
    "\n",
    "import ipywidgets as ipw\n",
    "fig, ((ax1,ax2,ax3)) = plt.subplots(1,3,figsize=(14,4), tight_layout=True)\n",
    "ax11 = ax1.twinx()\n",
    "ax22 = ax2.twinx()\n",
    "ax33 = ax3.twinx()\n",
    "axes = ax1, ax11, ax2, ax22, ax3, ax33\n",
    "axes = ax1, None, ax2, ax22, ax3, ax33\n",
    "def update(s1):\n",
    "    global Eps_record, Sig_record, iter_record, t_arr, s_t, s0, t0, Eps_n, axes\n",
    "    global margs\n",
    "    get_response_i(s1, margs)\n",
    "    Sig_arr = np.array(Sig_record, dtype=np.float_)\n",
    "    Eps_arr = np.array(Eps_record, dtype=np.float_)\n",
    "    iter_arr = np.array(iter_record,dtype=np.int_)\n",
    "    for ax in axes:\n",
    "        if ax:\n",
    "            ax.clear()\n",
    "    plot_Sig_Eps(t_arr, s_t, Sig_arr, Eps_arr, iter_arr, *axes)\n",
    "    \n",
    "init()\n",
    "\n",
    "s1_slider = ipw.FloatSlider(value=0,min=-4, max=+4, step=0.1,\n",
    "                                         continuous_update=False)\n",
    "\n",
    "ipw.interact(update, s1 = s1_slider);\n",
    "\n",
    "def reset(**material_params):\n",
    "    global margs\n",
    "    init()\n",
    "    s1_slider.value = 0\n",
    "    margs = [material_params[name] for name in py_vars]\n",
    "\n",
    "n_steps = 20\n",
    "margs_sliders = {\n",
    "    name : ipw.FloatSlider(description=name, value=val, \n",
    "                            min=minval, max=maxval, step=(maxval-minval) / n_steps,\n",
    "                           continuous_update=False)\n",
    "    for name, val, minval, maxval in [('E_b', 50, 0.5, 100),\n",
    "                                     ('gamma', 1, -20, 20),\n",
    "                                     ('K', 1, -20, 20),\n",
    "                                     ('tau_bar', 1, 0.5, 20)]\n",
    "}\n",
    "\n",
    "ipw.interact(reset, **margs_sliders);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "315.333px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
