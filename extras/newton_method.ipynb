{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **Appendix 2: Newton incremental iteration scheme**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To demonstrate the idea of a non-linear solver in simple terms, let us consider a known function defining the non-linear residuum. The principle assumption of the incremental time stepping scheme is that we know some initial value of the state variable that satisfies the governing equations, i.e. $R(u_0) = 0$.\n",
    "\n",
    "Then, we assume to know the derivatives of the sought function with respect to the state variable $u$. The time stepping algorithm provides us a means how to travel through the space of state variables along an admissible path satisfying the governing equations. The director of the travel is the pseudo-time variable $t$.\n",
    "\n",
    "In order to illustrate the concept, let us consider the simple function \n",
    "\\begin{equation}\n",
    "f(t)= \\mathrm{sin}(u(t))\n",
    "\\end{equation}\n",
    "Think of this equation as the equilibrium requirement of our mechanical model. On the left hand side, there is the prescribed history of loads $f(t)$. On the right hand side, the force that corresponds to the displacement of the structure $u$. Now, for prescribed history on the left hand side $f(t)$, we want to find the corresponding history of displacements $u(t)$ inducing force which is in equlibrium with the imposed load $f(t)$ at each time step of the loading history. \n",
    "\n",
    "Of course, we might solve this equation just by evaluating\n",
    "\\begin{align}\n",
    "u(t) = \\arcsin{f(t)}\n",
    "\\end{align}\n",
    "But we use the $\\sin$ function as a substitute for a structural response so that we pretend that an explicit evaluation is not possible. Therefore, we define the residuum equation which represents the lack of fit \n",
    "of the required equilibrium condition for each time instance $t$ as\n",
    "\\begin{equation} \\label{eq:residuum}\n",
    "R = \\mathrm{sin}(u(t)) - f(t) = 0.\n",
    "\\end{equation}\n",
    "In a numerical code, the value of the function $f(t)$ are evaluated in a prescribed sequence of incremental time steps $t_n, n = 0\\ldots N$.\n",
    "For each value of $t_n$ an iteration loop must be performed to find the value of $u$ satisfying the residuum.\n",
    "\n",
    "To get the numerical algorithm, we first approximate the residuum using the first two terms of the \n",
    "[Taylor series](https://en.wikipedia.org/wiki/Taylor_series) as\n",
    "\\begin{align}\n",
    "R(u^{k+1},t_n) = R(u^{k},t_n) + \\left.\\frac{ \\partial R(u) }{ \\partial u }\\right|_{u^k} \\Delta u^{k+1} = 0.\n",
    "\\end{align}\n",
    "\n",
    "In the considered case of $\\sin{u}$, the derivative of the residuum with respect to $u$ is calculated as\n",
    "\\begin{equation}\n",
    "\\left.\\frac{ \\partial R(u) }{ \\partial u } \\right|_{u^k} = \\mathrm{cos}(u^k)\n",
    "\\end{equation}\n",
    "\n",
    "The iteration loop can then be obtained by solving the expanded residuum for the  increment of the displacement  $\\Delta u^{k+1}$ as\n",
    "\\begin{equation}\n",
    "\\Delta u^{k+1} = - \\left[ \\left.\\frac{ \\partial R(u) }{ \\partial u }\\right|_{u^k} \\right]^{-1} R(u^k)\n",
    "\\end{equation}\n",
    "The new value of the  variable $u$ can be given as\n",
    "\\begin{equation}\n",
    "u^{k+1} = u^k + \\Delta u^{k+1} \n",
    "\\end{equation}\n",
    "\n",
    "The last two equations are repeated until the residuum is (almost) zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sinus function as a pseudo model of a nonlinear structural response \n",
    "\n",
    "To show the algorithm at work, we define the relation between pseudo displacement and pseudo force as a $\\sin$ function. To solve it iteratively, we have to supply the derivative of the function as well. In structural analysis, this derivative is identical to the instantaneous stiffness of the system. Also note, that the procedure works also for a force vector and stiffness matrix. Interesting historical context of the method and its wide range of applications are nicely summarized here: [Newton-Raphson method](https://en.wikipedia.org/wiki/Newton%27s_method)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(u):\n",
    "    \"\"\"Pseudo force\"\"\"\n",
    "    return np.sin(u)\n",
    "\n",
    "def df_du(u):\n",
    "    \"\"\"Pseudo algorithmic stiffness\"\"\"\n",
    "    return np.cos(u)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algorithm implementation\n",
    "Note that the algorithm is implemented as a double loop. The outer loop defines the load levels. The inner loop iterates over the values of residuum until the residuum gets smaller than the required threshold value prescribing the accuracy of the calculation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K_max = 100 # max number of iterations\n",
    "u = 0.0 #  initial value of displacement\n",
    "f_levels = np.linspace(0, 0.99999, 6) # prescribed load levels\n",
    "u_levels = []  # calculated displacement values for each load level\n",
    "for f_level in f_levels: # loop over the load levels\n",
    "    for K in range(K_max): # iteration loop\n",
    "        R = f(u) - f_level # value of the residuum\n",
    "        if np.fabs(R) < 1e-8: # residuum equal to zero?\n",
    "            break # stop iteration\n",
    "        dR = df_du(u) # derivative of the residuum\n",
    "        d_u = - R / dR # increment of displacement\n",
    "        u += d_u # update the total displacement       \n",
    "        if K == K_max - 1:\n",
    "            raise ValueError('No convergence')\n",
    "    print('number of iterations =',K)\n",
    "    u_levels.append(u) # record the found solution for current load level f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Print the obtained values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('u', u_levels)\n",
    "print('f', f_levels)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compare the obtained results with exact solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,1)\n",
    "fig.canvas.header_visible = False\n",
    "ax.plot(u_levels,f_levels,'o-')\n",
    "u_analytical = np.linspace(0, u_levels[-1], 500)\n",
    "ax.plot(u_analytical, f(u_analytical), linestyle='dashed');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Questions and tasks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1 What if the derivative is not available?\n",
    "Is it possible to trace the $\\sin(u)$ function even without the derivative? Sometimes it is difficult to obtain the derivative of the function so that other strategies are necessary. Examine the algorithm behavior by setting a value of the derivative, i.e.  $\\partial f(u) / \\partial u = \\cos(0)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2 Can the algorithm trace the descending branch of the function?\n",
    "Can the present algorithm trace the $\\sin(u)$ function also in the range $u > \\pi/2$. \n",
    "Why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3 Can the algorithm handle an infinite derivative?\n",
    "Use the algorithm above to trace the function $\\sqrt{u}$. Why doesn't it work? How to fix it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(u):\n",
    "    return np.sqrt(u)\n",
    "\n",
    "def df_du(u):\n",
    "    return 1./2./np.sqrt(u)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4 Can the algorithm handle a concave function?\n",
    "Use the algorithm above to find the solution to the functions $u^2$. Why doesn't it work? How to fix it? Use the algorithm in combination with the function below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(u):\n",
    "    return u**2 + .5*u\n",
    "\n",
    "def df_du(u):\n",
    "    return 2*u + .5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5 Visualization of the iteration process\n",
    "Apply an extnded version of the algorithm that records the iterations staps showing what the algorithm went through. Use the modified Newton method - using a constant derivative to see the all the loops that the algorithm went through. Don't forget to increase the number of iterations.\n",
    "<a id=\"newton_iteration_example\"></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K_max = 100 # max number of iterations\n",
    "u = 0 #  initial value of displacement\n",
    "f_t = np.linspace(0, 0.99999, 5) # prescribed load levels\n",
    "u_t = []  # calculated displacement values for each load level\n",
    "f_t_iter = [0.0] \n",
    "u_t_iter = [0.0] # interim values of displacement\n",
    "f_t_levels = [] # used for visualization of load levels\n",
    "u_t_levels = [] # used for visualization of load levels\n",
    "for f_level in f_t: # loop over the load levels\n",
    "    for K in range(K_max): # iteration loop\n",
    "        R = f(u) - f_level # value of the residuum\n",
    "        if np.fabs(R) < 1e-8: # residuum equal to zero?\n",
    "            break # stop iteration\n",
    "        dR = df_du(u) # derivative of the residuum\n",
    "        d_u = - R / dR # increment of displacement\n",
    "        u += d_u # update the total displacement\n",
    "        u_t_iter.append(u)# record data for visualization of iterations\n",
    "        f_t_iter.append(f_level)\n",
    "        u_t_iter.append(u) \n",
    "        f_t_iter.append(f(u))\n",
    "        f_t_levels.append([f_level,f_level]) \n",
    "        u_t_levels.append([0,u]) \n",
    "        if K == K_max - 1:\n",
    "            raise ValueError('No convergence')\n",
    "    print('number of iterations =',K)\n",
    "    u_t.append(u) # resord the found solution for current load level f\n",
    "    \n",
    "fig2, ax2 = plt.subplots(1,1)\n",
    "fig2.canvas.header_visible=False\n",
    "ax2.plot(u_t, f_t, 'o', color='black', lw=2) # plot solved increments as bullets\n",
    "ax2.plot(u_t_iter, f_t_iter, color='black') # plot iterations as solid liness\n",
    "ax2.plot(np.array(u_t_levels).T, np.array(f_t_levels).T, '-.',  \n",
    "                color='black', lw=1) # plot load levels as dash-dotted lines\n",
    "ax2.set_xlabel('w')\n",
    "ax2.set_ylabel('P(w)')\n",
    "ax2.set_xlim(0)\n",
    "ax2.set_ylim(0)\n",
    "u_analytical = np.linspace(0, u_levels[-1], 500)\n",
    "ax2.plot(u_analytical, f(u_analytical), linestyle='dashed', color='orange');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 6 Solve the problem using the newton method provided in scipy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.optimize import newton"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pdoc newton"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
