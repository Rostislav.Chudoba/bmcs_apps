# Specify parent image. Please select a fixed tag here.
ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:latest
#ARG BASE_IMAGE=registry.jupyter.rwth-aachen.de/profiles/rwth-minimal-2021-ss:latest
FROM ${BASE_IMAGE}
# .. Or update conda base environment to match specifications in environment.yml
ADD environment.yml /tmp/environment.yml
# All packages specified in environment.yml are installed in the base environment
RUN conda env update -f /tmp/environment.yml && \
    conda clean -a -f -y
