{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"top\"></a>\n",
    "# **4.1: Loading, unloading and reloading**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<!-- [![title](../fig/bmcs_video.png)](https://moodle.rwth-aachen.de/mod/page/view.php?id=551829) part 1 -->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo('SCuoGg7_8GE')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:lightgray;text-align:left\"> <img src=\"../icons/start_flag.png\" alt=\"Previous trip\" width=\"40\" height=\"40\">\n",
    "    &nbsp; &nbsp; <b>Starting point</b> </div> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have seen that there is a relation between the shape of the constitutive law and the  stress redistribution process within a bond zone which has an immediate consequence on the shape of the pullout curve, we extend our horizon to the case of **non-monotonic loading**.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:lightgray;text-align:left\"> <img src=\"../icons/destination.png\" alt=\"Previous trip\" width=\"40\" height=\"40\">\n",
    "    &nbsp; &nbsp; <b>Where are we heading</b> </div> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To motivate a more general development of the model for the bond-slip behavior let us consider the case of non-monotonic loading. What happens in the material structure of the bond if the load is reduced and then it grows again?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **Motivating examples**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider again the test double-sided pullout test introduced in [notebook 3.2](../tour3_nonlinear_bond/3_2_anchorage_length.ipynb#trc_pullout_study). Within this test series studying the nonlinear bond behavior of carbon fabrics, a loading scenario with introducing several **unloading and reloading steps** has been included for the specimen with the total length of 300 and 400 mm. The obtained measured response looked as follows \n",
    "![image](../fig/test_unloading.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:** Can the  pullout models from tours 2 and 3 be used to describe such kind of behavior? What constitutive law  can reproduce this kind of behavior? \n",
    "\n",
    "**Answer:** The bond-slip laws presented so far only considered monotonically increasing load and cannot reproduce the real material behavior upon unloading. \n",
    "The constant-bond slip model in Tour 2 and the multilinear bond-slip model exemplified in Tour 3 did not consider any change of behavior upon unloading. \n",
    "To document this statement and to show how to introduce unloading and reloading into the interactive numerical models  that we used in Tour 3, an example has been prepared showing how to introduce a more complex type of loading into the model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"trc_study_monotonic\"></a>\n",
    "### **Example 1:** TRC specimen with unloading\n",
    "\n",
    "Let us reuse the geometrical, material and algorithmic parameters already specified in the  \n",
    "[case study on carbon fabric bond](../tour3_nonlinear_bond/3_2_anchorage_length.ipynb#case_study_1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from bmcs_cross_section.pullout import PullOutModel1D\n",
    "po_trc = PullOutModel1D(n_e_x=100, w_max=3) # mm \n",
    "po_trc.geometry.L_x=150 # [mm]\n",
    "po_trc.time_line.step = 0.02\n",
    "po_trc.cross_section.trait_set(A_m=1543, A_f=16.7, P_b=10)\n",
    "po_trc.material_model='multilinear'\n",
    "po_trc.material_model_.trait_set(\n",
    "    E_m=28000, E_f=170000,\n",
    "    s_data = '0, 0.1, 0.5, 1, 2, 3,   4.5, 6', \n",
    "    tau_data = '0, 5.4, 4.9, 5, 6, 7, 8.3, 9'\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition, let us now change the `loading_scenario` to `cyclic` load.\n",
    "Note that this polymorphic attribute changes the 'shadowed' object\n",
    "which generates the time function for the scaling of the load. The \n",
    "shadowed object representing the cyclic loading scenario can then \n",
    "be accessed as an attribute `loading_scenario_`. It can be rendered \n",
    "using the `interact` method, like any other model component in the\n",
    "model tree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "po_trc.loading_scenario.profile = 'cyclic-nonsym-const'\n",
    "po_trc.loading_scenario.profile_.interact()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters of the loading scenario are now visible in the rendered window. They can be \n",
    "assigned through the `trait_set` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "po_trc.loading_scenario.profile_.trait_set(number_of_cycles=1,\n",
    "                              unloading_ratio=0.0,\n",
    "                               numbe_of_increments=200);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model can now be executed and rendered."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "po_trc.reset()\n",
    "po_trc.run()\n",
    "po_trc.interact()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apparently, there is no significant difference directly visible. However, if you browse through the history, it will be obvious that unloading is running along the same path as loading, which contradicts to the behavior observed in experiments."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **Example 2:** CFRP with unloading\n",
    "\n",
    "Even more evident demonstration of the non-physicality of the applied model can be provided by reusing the calibrated CFRP model. Let us apply the same loading scenario again"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A_f = 16.67 # [mm^2]\n",
    "A_m = 1540.0 # [mm^2]\n",
    "p_b = 100.0 #\n",
    "E_f = 170000 # [MPa]\n",
    "E_m = 28000 # [MPa]\n",
    "pm = PullOutModel1D()\n",
    "pm.material_model = 'multilinear'\n",
    "pm.material_model_.s_data = \"0, 0.1, 0.4, 4\"\n",
    "pm.material_model_.tau_data = \"0, 8, 0, 0\"\n",
    "pm.sim.tline.step = 0.01 # 100 time increments\n",
    "pm.cross_section.trait_set(A_f=A_f, P_b=p_b, A_m=A_m)\n",
    "pm.geometry.L_x = 300 # length of the specimen [mm]\n",
    "pm.w_max = 1.8 # maximum control displacement [mm]\n",
    "pm.n_e_x = 100 # number of finite elements\n",
    "pm.loading_scenario.profile = 'cyclic-nonsym-const'\n",
    "pm.loading_scenario.profile_.trait_set(number_of_cycles=2,\n",
    "                              unloading_ratio=0.0,\n",
    "                               numbe_of_increments=200)\n",
    "pm.run()\n",
    "pm.interact()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Browsing through the history will reveal that the debonded zone gets recovered upon unloading as the process zone travels back through the bond zone and the pullout tests gets completely healed once $w = 0$. This motivates the question: **How to introduce irreversible changes in the material structure** so that we reflect the physics behind the scenes in a correct way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"plasticity_and_damage\"></a>\n",
    "# **Physical explanation of the bond behavior**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<!-- [![title](../fig/bmcs_video.png)](https://moodle.rwth-aachen.de/mod/page/view.php?id=551829) part 2 -->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "YouTubeVideo('btj1SgUjBeA')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Regarding a small segment of the bond interface with a nearly constant shear stress $\\tau$ and constant \n",
    "slip $s$ let us try to describe the correspondence between the micro- and meso-scopic mechanisms that actually govern \n",
    "bond-slip relation $\\tau(s)$. To classify the elementary mechanisms behind the observed bond behavior, let us can idealize \n",
    "the material structure of the bond using two types of bindings:\n",
    "\n",
    " - as a **series of elastic springs** that can break once they achieve their strength, and \n",
    " - as a **series of asperities** representing an unevenness or roughness of the surface area. \n",
    "   Sliding of these surfaces is accompanied with a stress required to cross over the asperities. During this process the asperities can deform elastically or get abraded.\n",
    "\n",
    "With this simple image of the material interface we can try to associate the structure of the bond layer with the basic types of inelastic material behavior, namely, damage and plasticity. In most cases both types of material behavior occur. The question is, how to distinguish and quantify them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Damage:** Consider an infinitesimal material point of the interface exhibiting softening response upon monotonic loading. Upon unloading, this material point would unload to origin. This behavior can be reproduced by an idealization of the material point as a series of elastic springs. Assuming further a scatter of spring strength, the failure of one spring upon increasing slip displacement $s$ leads to the reduction of the bond stiffness and overall stress level $\\tau$. Since all the remaining springs are elastic, they would all unload to the origin."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![damage](../fig/damage.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Plasticity:**\n",
    "On the other hand, if a bond stress is transferred by asperities, we can distinguish two phases of response. Before the stress necessary to skip over an asperity has been reached, the slip is zero. At the onset of sliding over the asperities, the slip can increase at constant level of stress. Once the stress level is reduced, the slip does not change. Or putting it differently, the stress needed to induce sliding over the surface remains constant. Once the sliding direction is changed, also the sign of the shear stress must change. The described type of behavior is referred to as ideally plastic. This kind of material response is shown as the curve (2) \n",
    "![plasticity](../fig/plasticity.png)\n",
    "\n",
    "If asperities can deform before reaching the onset of sliding, an elastic deformation can be observed as indicated\n",
    "by the curve (3)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Interactions:** Usually, both damage and plasticity encounter simultaneously in the material behavior. Both introduce energy dissipation. While damage is connected with the reduction of the material stiffness, plasticity leads to permanent deformation after unloading of the structure. To understand the effect of damage and plasticity at the level of a material point, we will construct simple models that can be used within the BMCS interactive sheets to understand the two types of material behavior in detail."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Corresponding to this classification, we will address the two categories of behavior in a more\n",
    "detailed way:\n",
    "\n",
    "1. starting with interactive studies for non-monotonic loading scenarios, \n",
    "2. providing a mathematical framework for the description of plasticity and damage exemplified for the bond-behavior at a material point level\n",
    "3. studying the effect of plasticity and damage at the structural level using pullout tests."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:lightgray;text-align:left;width:45%;display:inline-table;\"> <img src=\"../icons/previous.png\" alt=\"Previous trip\" width=\"50\" height=\"50\">\n",
    "    &nbsp; <a href=\"../tour3_nonlinear_bond/3_2_anchorage_length.ipynb#top\">3.2 Anchorage length</a> \n",
    "</div><div style=\"background-color:lightgray;text-align:center;width:10%;display:inline-table;\"> <a href=\"#top\"><img src=\"../icons/compass.png\" alt=\"Compass\" width=\"50\" height=\"50\"></a></div><div style=\"background-color:lightgray;text-align:right;width:45%;display:inline-table;\"> \n",
    "    <a href=\"4_2_BS_EP_SH_I_A.ipynb#top\">4.2 Basic concept of plasticity</a>&nbsp; <img src=\"../icons/next.png\" alt=\"Previous trip\" width=\"50\" height=\"50\"> </div> "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
