{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 7.3 Softening function and damage function\n",
    "Using the softening function with embedded fracture energy derive a damage function that can be correctly embedded in a finite element calculation. Then, the softening is not applied to a zero-thickness decohesion plane but to a zone of a finite length. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To import the softening function derived in the notebook 7.2 run the notebook first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%run 7_2_Softening_law_with_embedded_fracture_energy.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_w"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to apply the softening function to a zone of length $L_s$?\n",
    "\n",
    "If we wish to embed the softening behavior into a finite element simulation we encounter the problem that the deformation is not described by the crack opening $w$. The finite element discretization assumes by definition a smooth stran field $verepilon$ and there is no notion of discontinuity. \n",
    "\n",
    "As a consequence, we need to account for a length of the softening zone  $L_s$ which is actually equivalent to the size of the finite element."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L_s, epsilon = sp.symbols('L_s, varepsilon')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The crack opening $w$ is simply substituted by\n",
    "\\begin{align}\n",
    " w = \\varepsilon L_\\mathrm{s}\n",
    "\\end{align}\n",
    "to obtain the softening function in terms of strains"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_epsilon = f_w.subs({'w' : epsilon * L_s})\n",
    "f_epsilon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the important fact that the integral of the softening function over the strains scales the dissipated energy by the term $1/L_\\mathrm{s}$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sp.integrate(f_epsilon, epsilon)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a consequence, by changing the size of the softening zone we also change the total amount of the energy dissipated!!! This feature of the softening function will be exploited in the implementation of the finite elements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to convert a softening function to damage function?\n",
    "\n",
    "The shape of the softening function describes the process of deterioration - starting from an undamaged state and ending in a fully damage state. Let us establish an equivalence between softening and damage evolution by requiring that they describe the same kind of stress decay.\n",
    "\n",
    "Considering the softening zone of the length $L_s$ let us describe the damage within this zone using the state variable $omega$ and the elastic modulus $E_c$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega, E_c = sp.symbols('omega, E_c')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, the constitute law to be used in a finite element of the zone is given as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma = (1 - omega) * E_c * epsilon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The stress decay described by the damage law and by the softening law should be the same. Let them set equal and solve for the damage variable $omega$. Using the sympy solver we obtain the algebraic solution as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega_Gf = sp.solve(sigma - f_epsilon, omega)[0]\n",
    "omega_Gf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This new damage function is defined using material parameters with a clear mechanical interpretation. Such kind of material law is attractive because it makes it possible to design tests that focus on an isolated phenomenon, i.e. the determination of the material strength, E modulus or fracture energy.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now visually verify the shape of the damage function.\n",
    "The set of parameters is assembled in a dictionary and then\n",
    "they are all substituted into the damage function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_f = dict(E_c = 28000, f_t = 3, L_s = 1, G_f = 0.01)\n",
    "omega_Gf_epsilon = omega_Gf.subs(data_f)\n",
    "omega_Gf_epsilon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The damage function is only valid in an inelastic regime. Therefore, we have to quantify the onset of inelasticity first as\n",
    "\\begin{align}\n",
    "\\varepsilon_0 = \\frac{f_t}{E_c}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "epsilon_0 = (f_t / E_c).subs(data_f)\n",
    "epsilon_0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the damage function can be plotted as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "splot(omega_Gf_epsilon, (epsilon,epsilon_0,epsilon_0*100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding stress strain curve has then the form"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma = sp.Piecewise( (E_c * epsilon, epsilon < epsilon_0),\n",
    "    (( 1 - omega_Gf ) * E_c * epsilon, epsilon >= epsilon_0))\n",
    "sigma"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "splot(sigma.subs(data_f), (epsilon,0,epsilon_0*100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how the stress strain function scales with the change of the fracture energy and of the zone length.\n",
    "\n",
    "Larger fracture energy makes the stress-strain response more ductile, while smaller makes it brittle.\n",
    "\n",
    "On the other hand, larger size of the softening zone makes the softening behavior more brittle and smaller size of the zone makes it more ductile. Why?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g_f = sp.integrate(sigma, epsilon)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g_f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "bmcs_env",
   "language": "python",
   "name": "bmcs_env"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
