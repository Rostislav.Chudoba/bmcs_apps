{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 7.2 Softening function and damage function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import sympy as sp\n",
    "import numpy as np\n",
    "import matplotlib.pylab as plt\n",
    "sp.init_printing()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us consider a softening function of the form\n",
    "\\begin{align}\n",
    "f(w) = c_1 exp( - c_2 w )\n",
    "\\end{align}\n",
    "This is function should describe the decay of stress starting from the material tensile strength and continuously deminishing to zero.\n",
    "The variable $w$ represents the crack opening and the parameters $c_1$ and $c_2$ are the material parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c_1, c_2 = sp.symbols('c_1, c_2', positive=True)\n",
    "w = sp.symbols(r'w')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = c_1 * sp.exp(-c_2*w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot the function to verify its shape for the material parameters set to the value 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_w = f.subs({c_1:1, c_2:1})\n",
    "f_w"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w_arr = np.linspace(0.01,10,100)\n",
    "fig.canvas.header_visible=False\n",
    "fig, ax = plt.subplots(1,1,figsize=(5,3), tight_layout=True)\n",
    "ax.plot(w_arr, sp.lambdify(w, f_w)(w_arr));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function can be already used in this form. The question is however, how to set the material parameters $c_1$ and $c_2$. They can be directly associated to a particular type of material parameters - namely, to the tensile strength $f_\\mathrm{t}$ and fracture energy $G_\\mathrm{f}$ "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_t, G_f = sp.symbols('f_t, G_f', positive=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Softening starts at the level of the material strength so that we can set $f(w = 0) = f_\\mathrm{t}$  to obtain the equation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Eq1 = sp.Eq(f.subs({'w':0}), f_t)\n",
    "Eq1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By solving for the $c_1$ we obtain the first substitution for our softening function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c_1_subs = sp.solve({Eq1}, c_1)\n",
    "c_1_subs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, if $w_0 = 0$, $c_1$ is equivalent to the tensile strength $f_\\mathrm{t}$\n",
    "\n",
    "The second possible mechanical interpretation is provided by the statement that softening directly represents the energy dissipation of a unit crack area. Thus, for large $w \\rightarrow \\infty$ it is equivalent to the energy producing a stress-free crack. This is the meaning of fracture energy.\n",
    "\n",
    "We can thus obtain the fracture energy represented by the softening function by evaluating its integral in the range $w \\in (0, \\infty)$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int_f_w = sp.integrate(f.subs(c_1_subs), w)\n",
    "int_f_w"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As $c_2 > 0$, only the second term matters.\n",
    "The determinate integral\n",
    "\\begin{align}\n",
    "\\left[ - \\frac{f_\\mathrm{t}}{c_2} \n",
    "\\exp(-c_2 w) \\right]_0^{\\infty}\n",
    "\\end{align}\n",
    "is zero for $w = \\infty$, so that the value in $w = 0$ delivers the result of the integral\n",
    "\\begin{align}\n",
    "\\frac{f_\\mathrm{t}}{c_2} \n",
    "\\end{align}\n",
    "\n",
    "This integral is equal to the fracture energy $G_\\mathrm{f}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E_0 = sp.symbols('E_0', positive=True ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Eq2 = sp.Eq(-int_f_w.subs({'w':0}), G_f)\n",
    "Eq2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and the value of $c_2$ delivers the second substitution for the softening function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c_2_subs = sp.solve({Eq2}, c_2)\n",
    "c_2_subs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The softening function with strength and fracture energy as a parameter now obtains the form"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_w = f.subs(c_1_subs).subs(c_2_subs)\n",
    "sp.simplify(f_w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify that the fracture energy is recovered at $w$ in infinity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sp.integrate(f_w, (w,0,sp.oo))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What if there is a finite elastic stiffness"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E_0, w_0 = sp.symbols(r'E_b, w_0', positive=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = c_1 * sp.exp(-c_2*w)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_w_0 = sp.Piecewise(\n",
    "    (1, w < w_0),\n",
    "    (f.subs(w, w-w_0), True)\n",
    ")\n",
    "f_w_0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sig_w = E_0 * w * f_w_0\n",
    "sig_w"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subs_c_1 = sp.solve( {sp.Eq( sig_w.subs(w, w_0), E_b * w_0 )}, {c_1} )\n",
    "subs_c_1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_f_ = sp.simplify( sp.integrate(sig_w.subs(subs_c_1), (w,0,sp.oo)) ).factor()\n",
    "G_f_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_, subs_c_2 = sp.solve( sp.Eq( G_f, G_f_ ), {c_2} )\n",
    "sp.simplify(subs_c_2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g1_w_ = sp.simplify( f_w_0.subs(c_2, subs_c_2).subs(c_1,1) )\n",
    "g1_w_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "g(\\tilde{s}) = \\displaystyle{\\frac{w_0}{\\tilde{w}}}\n",
    "\\exp\\left(\\frac{2 E_\\mathrm{b} w_0 ( \\tilde{w} - w_0) }\n",
    "{E_\\mathrm{b} w^2_0 - 2 G_\\mathrm{f}} \n",
    " \\right)\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g2_w_ = sp.Piecewise(\n",
    "    (1, w < w_0),\n",
    "    ( w_0 / w * sp.exp( \n",
    "    (2*E_b*w_0*(w-w_0))/(E_b*w_0**2-2*G_f) \n",
    "    ), True)\n",
    ")\n",
    "g2_w_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_g1_w_ = sp.lambdify( (w, w_0, E_b, G_f), g1_w_ )\n",
    "get_g2_w_ = sp.lambdify( (w, w_0, E_b, G_f), g2_w_ )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_g1_w_(0.1, 0.1, 10, 0.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,1)\n",
    "ax2 = ax.twinx()\n",
    "w_range = np.linspace(0.0001,1,100)\n",
    "ax.plot(w_range, get_g1_w_(w_range, 0.1, 10, 0.1), color='red')\n",
    "ax.plot(w_range, get_g2_w_(w_range, 0.1, 10, 0.1), color='blue')\n",
    "ax2.plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#f_t = E_b * s_0\n",
    "#g30_s_ =  f_t / E_b / s_equiv * sp.exp( - f_t / G_f_0 * (s_equiv - s_0) )\n",
    "#g3_s_ = sp.simplify(g30_s_.subs(G_f_0, G_f - E_b*s_0**2/2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to apply the softening function to a zone of length $L_s$?\n",
    "\n",
    "If we wish to embed the softening behavior into a finite element simulation we encounter the problem that the deformation is not described by the crack opening $w$. The finite element discretization assumes by definition a smooth stran field $verepilon$ and there is no notion of discontinuity. \n",
    "\n",
    "As a consequence, we need to account for a length of the softening zone  $L_s$ which is actually equivalent to the size of the finite element."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L_s, epsilon = sp.symbols('L_s, varepsilon')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The crack opening $w$ is simply substituted by\n",
    "\\begin{align}\n",
    " w = \\varepsilon L_\\mathrm{s}\n",
    "\\end{align}\n",
    "to obtain the softening function in terms of strains"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_epsilon = f_w.subs({'w' : epsilon * L_s})\n",
    "f_epsilon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the important fact that the integral of the softening function over the strains scales the dissipated energy by the term $1/L_\\mathrm{s}$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sp.integrate(f_epsilon.subs(L_s,1), (epsilon,0,sp.oo))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a consequence, by changing the size of the softening zone we also change the total amount of the energy dissipated!!! This feature of the softening function will be exploited in the implementation of the finite elements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to convert a softening function to damage function?\n",
    "\n",
    "The shape of the softening function describes the process of deterioration - starting from an undamaged state and ending in a fully damage state. Let us establish an equivalence between softening and damage evolution by requiring that they describe the same kind of stress decay.\n",
    "\n",
    "Considering the softening zone of the length $L_s$ let us describe the damage within this zone using the state variable $omega$ and the elastic modulus $E_c$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega, E_c = sp.symbols('omega, E_c')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, the constitute law to be used in a finite element of the zone is given as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma = (1 - omega) * E_c * epsilon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The stress decay described by the damage law and by the softening law should be the same. Let them set equal and solve for the damage variable $omega$. Using the sympy solver we obtain the algebraic solution as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega_Gf = sp.solve(sigma - f_epsilon, omega)[0]\n",
    "omega_Gf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma_v = (1- omega_Gf) * E_c * epsilon\n",
    "sigma_v"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This new damage function is defined using material parameters with a clear mechanical interpretation. Such kind of material law is attractive because it makes it possible to design tests that focus on an isolated phenomenon, i.e. the determination of the material strength, E modulus or fracture energy.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now visually verify the shape of the damage function.\n",
    "The set of parameters is assembled in a dictionary and then\n",
    "they are all substituted into the damage function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_f = dict(E_c = 28000, f_t = 3, L_s = 1, G_f = 0.01)\n",
    "omega_Gf_epsilon = omega_Gf.subs(data_f)\n",
    "omega_Gf_epsilon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The damage function is only valid in an inelastic regime. Therefore, we have to quantify the onset of inelasticity first as\n",
    "\\begin{align}\n",
    "\\varepsilon_0 = \\frac{f_t}{E_c}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "epsilon_0 = (f_t / E_c).subs(data_f)\n",
    "epsilon_0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the damage function can be plotted as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "splot(omega_Gf_epsilon, (epsilon,epsilon_0,epsilon_0*100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding stress strain curve has then the form"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma = sp.Piecewise( (E_c * epsilon, epsilon < epsilon_0),\n",
    "    (( 1 - omega_Gf ) * E_c * epsilon, epsilon >= epsilon_0))\n",
    "sigma"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "splot(sigma.subs(data_f), (epsilon,0,epsilon_0*100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how the stress strain function scales with the change of the fracture energy and of the zone length.\n",
    "\n",
    "Larger fracture energy makes the stress-strain response more ductile, while smaller makes it brittle.\n",
    "\n",
    "On the other hand, larger size of the softening zone makes the softening behavior more brittle and smaller size of the zone makes it more ductile. Why?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g_f = sp.integrate(sigma, epsilon)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g_f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "bmcs_env",
   "language": "python",
   "name": "bmcs_env"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
